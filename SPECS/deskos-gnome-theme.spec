%global upstream_repo horst3180
%global upstream_name arc-theme
%global commit 8290cb813f157a22e64ae58ac3dfb5983b0416e6
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           deskos-gnome-theme
Version:        0.4
Release:        1%{?dist}
Summary:        DeskOS default theme for GTK 3 and GNOME Shell

Group:          User Interface/Desktops
License:        GPLv3
URL:            https://github.com/deskosproject/deskos-gnome-theme-rpm
Source0:        https://github.com/%{upstream_repo}/%{upstream_name}/archive/%{commit}/%{upstream_name}-%{shortcommit}.tar.gz
Patch0:         deskos-name.patch

BuildArch:      noarch
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  gtk3-devel
Requires:       gnome-themes-standard
Requires:       gtk-murrine-engine

%description
DeskOS default theme based on Arc, a flat theme with transparent
elements for GTK 3, GTK 2 and Gnome-Shell.

%prep
%setup -q -n %{upstream_name}-%{commit}
%patch0 -p1

%build
./autogen.sh --prefix=/usr \
             --disable-darker \
             --disable-dark \
             --disable-cinnamon \
             --disable-gnome-shell \
             --disable-unity \
             --disable-xfwm \
             --disable-xfce-notify \
             --with-gnome=3.22

%install
make install DESTDIR=$RPM_BUILD_ROOT

find ${RPM_BUILD_ROOT} -name "*.sh" -exec chmod -x {} \;

%files
%defattr(-,root,root)
%doc
%{_datadir}/themes/DeskOS/gtk-2.0/
%{_datadir}/themes/DeskOS/gtk-3.0/
%{_datadir}/themes/DeskOS/metacity-1/
%{_datadir}/themes/DeskOS/index.theme

%changelog
* Mon Oct 23 2017 Ricardo Arguello <rarguello@deskosproject.org> - 0.4-1
- Updated to git commit 8290cb813f157a22e64ae58ac3dfb5983b0416e6

* Wed Mar 22 2017 Ricardo Arguello <rarguello@deskosproject.org> - 0.3-1
- Updated to release 20170302

* Fri Dec 16 2016 Ricardo Arguello <rarguello@deskosproject.org> - 0.2-1
- Updated to release 20161119

* Thu May 12 2016 Ricardo Arguello <rarguello@deskosproject.org> - 0.1-1
- Initial release
